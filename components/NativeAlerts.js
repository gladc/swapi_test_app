import React, {Component} from 'react';
import {Alert} from 'react-native';


export default class NativeAlerts extends Component {

    static showDialogWithErrorMessage = (title, message) => {
        Alert.alert(
            title,
            message,
            [
                {text: 'OK'},
            ],
            {cancelable: false}
        )
    };
}