import React, {Component} from 'react';
import {ActivityIndicator, Picker, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Table, Row, TableWrapper, Cols} from 'react-native-table-component';
import * as ConverterHelper from '../helpers/ConverterHelper'
import NativeAlerts from "./NativeAlerts";
import * as AxiosFunctions from '../actions/AxiosFunctions'

export class List extends Component {
    constructor(props) {
        super(props);

        const elementButton = (buttonIndex, buttonLabel) => (
            <TouchableOpacity onPress={() => this.sortTable(buttonIndex, this.state.shownCharacters)}>
                <View style={styles.button}>
                    <Text>{buttonLabel}</Text>
                </View>
            </TouchableOpacity>
        );

        this.state = {
            widthArr: [130, 130, 130],
            shownCharacters: [],
            allCharacters: [],
            genderList: [],
            animating: true,
            gender: '',
            columnSorted: '',
            tableHead: [
                [elementButton('0', 'Name')],
                [elementButton('1', 'Gender')],
                [elementButton('2', 'Height')]
            ]
        }
    }


     componentDidMount() {
         this.getCharacterTraits(this.props.characters)
    }

    getCharacterTraits(movieURLS) {
        const gender = []
        movieURLS.map((URL) => {
            AxiosFunctions.fetchInfo(URL)
                .then((response)=>{
                        const rowData = []
                        rowData.push(`${response.name}`);
                        rowData.push(`${response.gender}`);
                        rowData.push(`${ConverterHelper.convertCmToFeet(response.height)}`);
                        gender.push(`${response.gender}`);
                        this.setState({
                            shownCharacters: this.state.shownCharacters.concat([rowData]),
                            allCharacters: this.state.allCharacters.concat([rowData])
                        })
                            this.getGenders(gender)
                })
                .catch(()=>{
                    NativeAlerts.showDialogWithErrorMessage("Error", 'Could not retrieve character traits')
                        this.setState({animating: false,})
                })
        })
    }

    getGenders(duplicateGenders) {
        let genderList = []
        for (let i = 0; i < duplicateGenders.length; i++) {
            if (!genderList.includes(duplicateGenders[i])) {
                genderList.push(`${duplicateGenders[i]}`)
            }
        }
        this.setState({genderList: genderList, render: true, animating: false})
    }

    filterTable(selectedGender) {
        let filteredCharacters
        filteredCharacters = this.state.allCharacters.filter(function (character) {
            if (character.includes(selectedGender)) return character
        })
        this.setState({
            shownCharacters: filteredCharacters,
            animating: false
        })
    }

    sortAscending(buttonIndex, array) {
        this.setState({
            columnSorted: buttonIndex,
            shownVisibleCharacters:
                array.sort(function (a, b) {
                    if (buttonIndex == 2) {
                        return parseInt(a[buttonIndex], 10) - parseInt(b[buttonIndex], 10)
                    } else {
                        if (a[buttonIndex] < b[buttonIndex]) {
                            return -1;
                        }
                        if (a[buttonIndex] > b[buttonIndex]) {
                            return 1;
                        }
                        return 0;
                    }
                })
        })
    }

    sortDescending(buttonIndex, array) {
        this.setState({
            columnSorted: buttonIndex,
            shownVisibleCharacters:
                array.reverse(function (a, b) {
                    if (buttonIndex == 2) {
                        return parseInt(a[buttonIndex], 10) - parseInt(b[buttonIndex], 10)
                    } else {
                        if (a[buttonIndex] < b[buttonIndex]) {
                            return -1;
                        }
                        if (a[buttonIndex] > b[buttonIndex]) {
                            return 1;
                        }
                        return 0;
                    }
                })
        })
    }

    heightsSum() {
        let sum = 0
        this.state.shownCharacters.map((m) => {
            sum += parseInt(m[2], 10)
        })
        return ConverterHelper.convertCmToFeet(sum)
    }

    sortTable(buttonIndex, array) {
        if (this.state.columnSorted !== buttonIndex) {
            this.sortAscending(buttonIndex, array)
        } else {
            this.sortDescending(buttonIndex, array)
        }
        this.heightsSum()
    }

    handleChangeOption(selectedGender) {
        if (selectedGender) {
            this.setState({gender: selectedGender, animating: true})
            this.filterTable(selectedGender)
            this.heightsSum()
        }
        else {
            this.setState({
                shownCharacters: this.state.allCharacters,
                columnSorted: '',
                gender: ''
            })
        }
    }

    render() {
        const activityIndicator =
            <ActivityIndicator
                animating={this.state.animating}
                color='#000000'
                size="large"
                style={styles.activityIndicator}/>
        return (
            <View style={styles.container}>
                {activityIndicator}
                {this.state.render ? <View>
                    <View style={styles.pickerContainer}>
                        <Picker
                            selectedValue={this.state.gender}
                            onValueChange={(itemValue) => {
                                this.handleChangeOption(itemValue);
                            }}>
                            <Picker.Item key='' label='All characters' value=''/>
                            {this.state.genderList.map(function (elem, index) {
                                return (<Picker.Item label={elem} value={elem} key={index}/>);
                            })}
                        </Picker>
                    </View>
                        <TableWrapper>
                            <Cols data={this.state.tableHead} textStyle={styles.text}/>
                        </TableWrapper>
                        <Table>
                            {this.state.shownCharacters.map((rowData, index) => (
                                <Row
                                    key={index}
                                    data={rowData}
                                    widthArr={this.state.widthArr}
                                    style={[styles.row]}
                                    textStyle={styles.text}
                                />
                            ))}
                        </Table>
                        <View style={styles.textContainer}>
                            <Text>
                                Characters shown {this.state.shownCharacters.length}
                            </Text>
                            <Text>
                                Total height {this.heightsSum()}
                            </Text>
                        </View>
                </View> : null}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginBottom: 30,
        marginTop: 30
    },
    text: {
        textAlign: 'center',
        fontWeight: '100'
    },
    textContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: -1,
        borderColor: 'black',
        borderWidth: 1,
        height: 40,
        textAlign: 'center',
        fontWeight: '100'
    },
    row: {
        height: 40
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: -1,
        marginRight: -1,
        marginBottom: -1,
        borderWidth: 1,
        height: 40,
        textAlign: 'center',
        fontWeight: '100',
    },
    pickerContainer: {
        marginBottom: -1,
        borderWidth: 1,
        borderColor: 'black'
    },
});