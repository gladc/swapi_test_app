

export function convertCmToFeet(height) {
    let heightInFeet = height / 30.48
    let integerPart = Math.floor(heightInFeet);
    let heightInInches = Number.parseFloat((heightInFeet % 1) * 12).toFixed(2);
    return height + ' cm./ ' + integerPart + ' ft.' + ' ' + heightInInches + ' inches'
}