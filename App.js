import React, {Component} from "react";
import {MainScreen} from "./ui/screen/MainScreen.js"
import {createStackNavigator} from 'react-navigation'

export class App extends Component {}

export default createStackNavigator({
    Start: {
        screen: MainScreen,
        navigationOptions: {
            header: null,
        }
    },
});

