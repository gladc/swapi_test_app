import React, {Component} from 'react';
import {ActivityIndicator, Image, Picker, ScrollView, StyleSheet, Text, View} from 'react-native';
import {List} from "../../components/List";
import * as AxiosFunctions from '../../actions/AxiosFunctions'
import NativeAlerts from '../../components/NativeAlerts'

const URL = `https://swapi.co/api/films/`;

export class MainScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedMovieURL: '',
            films: [],
            allCharacterURL: [],
            animating: true,
            render: false,
            selectedMovieOpeningCrawl: ''

        };
    }

    handleChangeOption = (movieURL) => {
        if (movieURL) {
            this.setState({
                render: false,
                animating: false,
                selectedMovieURL: movieURL
            }, () => {
                AxiosFunctions.fetchInfo(movieURL).then((response) => {
                    this.setState({
                        selectedMovieOpeningCrawl: response.opening_crawl,
                        allCharacterURL: response.characters,
                        animating: false,
                        render: true
                    })
                }).catch(() => {
                    NativeAlerts.showDialogWithErrorMessage("Error", 'Could not retrieve movie information')
                    this.setState({animating: false})
                })
            });
        } else {
            this.resetState()
        }
    }

    resetState() {
        this.setState({
            selectedMovieURL: '',
            selectedMovieOpeningCrawl: '',
            allCharacterURL: [],
            render: false
        })
    }


    componentDidMount() {
        AxiosFunctions.fetchInfo(URL).then((response) => {
            this.setState({
                films: response.results.sort(function (a, b) {
                    let dateA = new Date(a.release_date), dateB = new Date(b.release_date);
                    return dateA - dateB;
                }),
                animating: false
            })
        }).catch(() => {
            NativeAlerts.showDialogWithErrorMessage("Error", 'Could not retrieve movie list')
        })
    }

    render() {
        const activityIndicator =
            <ActivityIndicator
                animating={this.state.animating}
                color='#000000'
                size="large"
                style={styles.activityIndicator}/>

        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.container}>
                        <View style={styles.pickerContainer}>
                            <Picker
                                selectedValue={this.state.selectedMovieURL}
                                onValueChange={(itemValue) => {
                                    this.handleChangeOption(itemValue);
                                }}>
                                <Picker.Item key='' label='Select a movie' value=''/>
                                {this.state.films.map((movie) => {
                                    return (<Picker.Item label={movie.title} value={movie.url}
                                                         key={movie.episode_id}/>);
                                })}
                            </Picker>
                        </View>
                        {activityIndicator}
                        <View style={styles.textContainer}>
                            <Text>
                                {this.state.selectedMovieOpeningCrawl}
                            </Text>
                        </View>

                        {!this.state.selectedMovieOpeningCrawl.length ?
                            <Image
                                style={styles.logo}
                                resizeMode="contain"
                                source={require('../assets/Star-Wars-Logos-icons-Vector.png')}
                            /> : null}
                        {this.state.render ?
                            <List characters={this.state.allCharacterURL}
                            /> : null}
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFD700',
    },
    logo: {
        height: 200,
        width: 200,
    },
    textContainer: {
        marginTop: 10,
        marginBottom: 10
    },
    activityIndicator: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        height: 80
    },
    pickerContainer: {
        marginTop: 50,
        borderWidth: 1,
        borderColor: 'black',
        height: 50,
        width: 300
    },
});
