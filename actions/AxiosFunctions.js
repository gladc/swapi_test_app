import axios from "axios";


export function fetchInfo(URL) {
    return axios.get(URL)
        .then(response => {
            return Promise.resolve(response.data)
        })
        .catch(error => Promise.reject(error))
}